<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('players.index');
});
/*Route::get('show', 'PlayersController@ShowPlayers');
Route::get('add-player', 'PlayersController@ShowForm');
Route::post('add-player', 'PlayersController@AddPlayer');
Route::put('player.update', 'PlayersController@EditPlayer');
Route::delete('delete-player/{id}', 'PlayersController@DeletePlayer');*/
Route::resource('players', 'PlayersController');
Route::get('players/{id}/destroy', 'PlayersController@destroy')->name('players.destroy');
