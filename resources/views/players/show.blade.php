@extends('template.template')
@section('title')
    Registro Exitoso
@endsection
@section('content')
<div class="jumbotron jumbotron-fluid bg-dark">
        <div class="container">
            <h1 class="display-4 text-white">Registro Exitoso del jugador</h1>
            <h1 class="text-white">{{$player->name_player}} {{$player->lastname_player}}</h1>
        </div>
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="container">
                        <div class="row">
                            <h1 class="text-white">Nombre del Equipo : </h1>
                            <h1 class="text-white">{{$player->team_player}}</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection