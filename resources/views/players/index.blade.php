@extends('template.template')
@section('title')
    Lista de Jugadores
@endsection
@section('content')
    <div class="jumbotron jumbotron-fluid bg-dark">
        <div class="container">
            <h1 class="display-4 text-white">Jugadores Registrados</h1>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Apellido</th>
                    <th scope="col">Equipo</th>
                    <th scope="col">Acción</th>
                </tr>
            </thead>
            <tbody>
                @if($players->isEmpty())
                    <th scope="row">No hay registros que mostrar</th>
                    @else 
                    @foreach($players as $player)
                        <tr>
                            <th scope="row">{{$player->id}}</th>
                            <td>{{$player->name_player}}</td>
                            <td>{{$player->lastname_player}}</td>
                            <td>{{$player->team_player}}</td>
                            <td>
                                <a href="{{route('players.edit', $player->id)}}" class="btn btn-primary">Modificar</a>
                                <a 
                                href="{{route('players.destroy', $player->id)}}" 
                                onclick="return confirm('¿Deseas elimminar el jugador seleccionado?')"
                                class="btn btn-danger">Eliminar
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
            </table>
        </div>
    </div>
@endsection