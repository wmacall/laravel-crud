@extends('template.template')
@section('title')
    Añadir Jugador
@endsection
@section('content')
<div class="col-md-6 col-md-offset-3">
    <div class="panel panel-default">
        <div class="panel-body">
        <form action="{{route('players.store')}}" method="POST">
            {{csrf_field()}}
            <label for="name_player">Nombre del Jugador</label>
            <input type="text" class="form-control" name="name_player" placeholder="Ingresa el Nombre">
            <label for="lastname">Apellido del Jugador</label>
            <input type="text" class="form-control" name="lastname_player" placeholder="Ingresa el Apellido">
            <label for="team">Equipo del Jugador</label>
            <input type="text" class="form-control" name="team_player" placeholder="Ingresa el Equipo">
            <hr>
            <button type="submit" class="btn btn-primary">Agregar Jugador</button>
            <hr>
        </form>
        </div>
    </div>
</div>

@endsection