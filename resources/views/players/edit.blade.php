@extends('template.template')
@section('title')
    Modificar Jugador
@endsection
@section('content')
{!! Form::open(['route' => ['players.update', $player], 'method' =>'PUT']) !!}
    <div class="container">
    <h1>Ingresa los nuevos datos de: {{$player->name_player}} </h1>
        <div class="form-group">
            {!! Form::label('name', 'Nombre') !!}
            {!! Form::text('name_player', $player->name_player, ['class' => 'form-control', 'placeholder' => 'Nombre del Jugador']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('name', 'Apellido') !!}
            {!! Form::text('lastname_player', $player->lastname_player, ['class' => 'form-control', 'placeholder' => 'Apellido del Jugador']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('name', 'Equipo') !!}
            {!! Form::text('team_player', $player->team_player, ['class' => 'form-control', 'placeholder' => 'Equipo del Jugador']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Guardar Modificaciones', ['class' => 'btn btn-primary']) !!}
        </div>
    </div>
{!! Form::close() !!}
@endsection